from distutils.core import setup
from setuptools import find_packages
setup(
    name='buket_theme',
    version='0.9.8',
    author='Oksana Zaprozhets',
    author_email='zap.oksana@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    entry_points={
        'sshop.plugins.settings.entrypoints': [
            'update_settings = buket_theme:update_settings',
            'install = buket_theme:install',
            'upgrade = buket_theme:upgrade',
        ],
    },  
)
