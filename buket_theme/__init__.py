# coding: utf-8

def update_settings(inherited_settings):
    new_settings = {}
    new_settings['AVAILABLE_SHOP_THEMES'] = inherited_settings['AVAILABLE_SHOP_THEMES'] + (('buket_theme', u'buket_theme'),)
    return new_settings

def install(project_dir=''):
    import subprocess,os
    CMDS = [
        'python %s collectstatic --noinput' % os.path.join(project_dir, '../manage.py'),
        'python %s compress' % os.path.join(project_dir, '../manage.py'),
    ]
    for c in CMDS:
        subprocess.call(c, shell=True)

def upgrade(from_version, to_version, project_dir=''):
    import subprocess,os
    CMDS = [
        'python %s collectstatic --noinput' % os.path.join(project_dir, '../manage.py'),
        'python %s compress' % os.path.join(project_dir, '../manage.py'),
    ]
    for c in CMDS:
        subprocess.call(c, shell=True)
    #print 'Update from', from_version, ' to ', to_version
    pass